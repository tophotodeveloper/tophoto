domainName=topho.to
number=$(echo $1 | python3 ./padInput.py)
url=https://$domainName/100/$number
localUrl=http://127.0.0.1:8000/100/IMG_$number.JPG

termux-dialog confirm -i "image $number" -t "is this the correct filename?" > output.json
dialogChoice=$(python3 ./parseJson.py)

if [ "$dialogChoice" != "no" ] 
then
	echo "image confirmed"
else
	termux-toast -c red -s "stopping"
	exit
fi


if [ "$2" != "skip" ]
then
	termux-toast -c green -s "proceeding, open camera app"
	python3 main.py
	termux-toast -c yellow -s "image ready, open terminal (5 SECONDS)"
	sleep 7
fi

termux-dialog confirm -t "preview locally?"  > output.json
dialogChoice=$(python3 ./parseJson.py)

if [ "$dialogChoice" != "no" ] 
then
	echo "image confirmed"
	termux-open-url $localUrl
	termux-toast -g top -c yellow -s 10 seconds to open app back up
	sleep 5
	termux-toast -c yellow -s 5 seconds, close browser
	sleep 5
fi

termux-dialog confirm -i "YES to keep file, NO to delete" -t "Need it or Keep it?" > output.json
dialogChoice=$(python3 ./parseJson.py)


# termux-toast -s $dialogChoice

if [ "$dialogChoice" != "no" ] 
then
	termux-toast -c green -s "NEED IT"
	git add ./image_directory/100/IMG_$number.JPG
	git add ./output_dir/100/IMG_$number.JPG
	git add ./output_dir/100/$number.html
	git status
	git commit -m "auto push $url"
	git push
else
	termux-toast -c red -s "KEEP IT"
	rm -rf ./image_directory/100/IMG_$number.JPG
	rm -rf ./output_dir/100/IMG_$number.JPG
	rm -rf ./output_dir/100/$number.html
	git add --all
	git status
	git commit -m "auto deletion of $url"
	git push
	exit
fi

termux-toast -g top -s sleeping for 30 seconds

sleep 30

termux-toast -g top -s checking $url

statusCode=$(curl -o /dev/null --silent --head --write-out '%{http_code}\n' $url)
# statusCode="200"

if [ "$statusCode" != "200" ]
then
	termux-toast -c red -s "$statusCode 15s timeout"
	sleep 15
	termux-toast -s "done sleeping, trying again"
	rm -rf ./output.json
	$0 $1 skip
else
	rm -rf ./output.json
	termux-toast -g bottom -c green "SUCCESS : $statusCode"
	termux-notification -c "SCAN THAT SHIT"
	termux-open-url $url
	$0 $(($1 + 1))
fi
