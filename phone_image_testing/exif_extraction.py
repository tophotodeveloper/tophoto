from typing import Tuple
import exif

def convert_tuple_to_gps_string(t):
    '''41°24'12.2"N'''
    return f"{t[0]}°{t[1]}'{t[2]}\""

def tuple_to_DMM(t):
    return f"{t[0]} {t[1]}"

def google_maps_url(lat, long):
    return f"https://www.google.com/maps/place/{lat[0]}%C2%B0{lat[1]}'{lat[2]}%22N+7%C2%B041'00.0%22E"

im_path = "TERM_20220326_090936_NR.jpg"

im = None

with open(im_path, "rb") as fo:
    im = exif.Image(fo)

lat = im.gps_latitude
long = im.gps_longitude



lat_long = f"{tuple_to_DMM(lat)}, {tuple_to_DMM(long)}"

breakpoint()
