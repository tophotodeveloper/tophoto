from jinja2 import Environment, FileSystemLoader
import os
import shutil
import pathlib
from time import sleep

development_fast_render = True 

jinja_ext = "jinja"

html_output_file_path = f'./output_dir'
path_to_jinja_html_templates = './jinja_templates'
image_directory = "./image_directory"

generate_site_path = lambda directory, image : f"{html_output_file_path}/{directory}/{image}.html"

sitemap_url = "https://topho.to"

templates_to_render = [
    f"index"
]

def render_all(image_dir=image_directory):
    for file in templates_to_render:
        with open(f"{html_output_file_path}/{file}.html", 'w', encoding="utf-8") as fo:
            template = env.get_template(f"{file}.jinja")
            fo.write(
                template.render()
            )

    image_subdirectories = os.listdir(image_directory)
    output_directories = [s[:3] for s in image_subdirectories]


    output_subdirectories = [s for s in os.listdir(html_output_file_path) if s in output_directories]

    if output_subdirectories != output_directories:
        for dir in output_directories:
            try:
                os.mkdir(f"{html_output_file_path}/{dir}")
            except FileExistsError:
                pass

    for dir in output_subdirectories:
        # images = os.listdir(f"{image_directory}/{dir}EOS7D")
        images = os.listdir(f"{image_directory}/{dir}")
        for image in images:
            with open(f"{html_output_file_path}/{dir}/{image[:-4].replace('IMG_', '')}.html", "w") as fo:
                template = env.get_template('image_page.jinja')
                fo.write(
                    template.render(filename=image, directory_name=dir)
                )        
            #im = Image.open(f"{image_directory}/{dir}EOS7D/{image}")
            #im.save(f"{html_output_file_path}/{dir}/{image}", optimize=True, quality=60)
            shutil.copy(
                f"{image_directory}/{dir}/{image}",
                f"{html_output_file_path}/{dir}"
            )

def find_last_modified_times(jinja_path=path_to_jinja_html_templates):
    a = [pathlib.Path(f'{jinja_path}/{x}').stat().st_mtime for x in tuple(os.walk(jinja_path))[0][2]]
    b = [pathlib.Path(f'{image_directory}')]

def generate_sitemap(site_directory=html_output_file_path + "/100", output_file=f"{html_output_file_path}/sitemap.txt"):
    with open(output_file, 'w') as fo:
        fo.write(f'\n{sitemap_url}/100/'.join([file for file in os.listdir(site_directory) if file.endswith(".html") and not file.startswith("base")]))
    print(f"sitemap generated and outputed to {output_file}")

def determine_next_filename(number_of_files_in_dir : int) -> str:
    return f"IMG_{str(number_of_files_in_dir).zfill(4)}.JPG"

if __name__ == '__main__':
    env = Environment(loader=FileSystemLoader(path_to_jinja_html_templates))
    dir_to_watch = "./image_directory/100"

    render_all()

    last_modified_times = find_last_modified_times() 

    # generate_sitemap()

    print("Rendered Jinja, waiting for changes. . .")

    files_in_folder = os.listdir(dir_to_watch)

    # print("waiting for changes . . .")

    while(True):
        sleep(0.2)
        temp_files_in_folder = os.listdir(dir_to_watch)
        if temp_files_in_folder != files_in_folder:
            print("new files(s) detected, going to sleep to let open camera do it's thing for a sec. . .")
            sleep(4)
            print("done sleeping")
            file_to_delete = [f for f in temp_files_in_folder if "IMG_" not in f][0]
            print(f"new file {file_to_delete} detected. . .")
            new_filename = determine_next_filename(len(temp_files_in_folder))
            with open(f"{dir_to_watch}/{file_to_delete}", "rb") as infile:
                with open(f"{dir_to_watch}/{new_filename}", "wb") as outfile:
                    outfile.write(infile.read())
                    print(f"saved {new_filename}")
            print(f"removing {file_to_delete}")
            os.remove(f"{dir_to_watch}/{file_to_delete}")
            print("updating file list. . .")
            files_in_folder = os.listdir(dir_to_watch)
            print(f"rendering site . . .")
            render_all()
            # print(f"\n\n All done, waiting for changes again. . .")
            print(f"\n\n All done, exiting. . .")
            exit()
    '''
    while True:
        sleep(0.3)
        if find_last_modified_times() != last_modified_times:
            print("change detected, re-rendering. . .")
            generate_sitemap()
            last_modified_times = find_last_modified_times()
            sleep(0.1)
            render_all()
    '''
