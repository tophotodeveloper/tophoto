import os

from time import sleep

dir_to_watch = "./image_directory/103"

def determine_next_filename(number_of_files_in_dir : int) -> str:
    return f"IMG_{str(number_of_files_in_dir).zfill(4)}.JPG"

if __name__ == "__main__":
    files_in_folder = os.listdir(dir_to_watch)

    print("waiting for changes . . .")

    while(True):
        sleep(0.2)
        temp_files_in_folder = os.listdir(dir_to_watch)
        if temp_files_in_folder != files_in_folder:
            print("new files(s) detected, going to sleep to let open camera do it's thing for a sec. . .")
            sleep(4)
            print("done sleeping")
            file_to_delete = [f for f in temp_files_in_folder if "IMG_" not in f][0]
            print(f"new file {file_to_delete} detected. . .")
            new_filename = determine_next_filename(len(temp_files_in_folder))
            with open(f"{dir_to_watch}/{file_to_delete}", "rb") as infile:
                with open(f"{dir_to_watch}/{new_filename}", "wb") as outfile:
                    outfile.write(infile.read())
                    print(f"saved {new_filename}")
            print(f"removing {file_to_delete}")
            os.remove(f"{dir_to_watch}/{file_to_delete}")
            print("updating file list. . .")
            files_in_folder = os.listdir(dir_to_watch)
            print(f"\n\n All done, waiting for changes again. . .")